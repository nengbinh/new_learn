var bars = d3.select('body').selectAll('div').data([15,32,10,8,60,24])

bars.enter()
    .append('div')
    .style('background-color', d => 'rgb(0,'+d*4+',0)' )
    .style('width', d => d * 4 + 'px' )
    .text( d => d )
