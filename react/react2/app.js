const ptag = React.createElement('h2', null, 'Hello Dojo!');
const ptag1 = React.createElement('h3', null, 'things I need to do')
const litag1 = React.createElement('li', null, 'Learn React')
const litag2 = React.createElement('li', null, 'Climb Mt. Everest')
const litag3 = React.createElement('li', null, 'Run a marathon')
const litag4 = React.createElement('li', null, 'Feed the dogs')
const ultag = React.createElement('ol', null, [litag1,litag2,litag3,litag4])

ReactDOM.render([ptag,ptag1,ultag], document.getElementById('app'));
