const Title = (props) => {
    const { text } = props; // A little bit of destructuring
    return React.createElement('h1', null, text);
}
const App = (props) => {
    return React.createElement(Title, { text: 'Dynamic content! Hooray!' } );
}


function CheckerBoard (lines) {
    let arr = []
    let num = 0
    for(let i = 0; i < lines; i++) {
        arr.push( React.createElement('br', null ) )
        num ++
        for(let j = 0; j < lines; j++) {
            if (num % 2) {
                arr.push( React.createElement(Cell_Row, { style: { style: styles.cell_b } } ) )
            }else{
                arr.push( React.createElement(Cell_Row, { style: { style: styles.cell_r } } ) )
            }
            num ++
        }
    }
    return arr
}
function Cell_Row (props) {
    const { style } = props;
    return React.createElement('div', style);
}
// And feel free to use the following object:
var styles = {
    cell_b: {height: '20px', width: '20px', display:'inline-block', backgroundColor: 'black'},
    cell_r: {height: '20px', width: '20px', display:'inline-block', backgroundColor: 'red'},
}

ReactDOM.render( CheckerBoard(12), document.getElementById('app') );
