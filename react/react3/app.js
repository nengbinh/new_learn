ReactDOM.render(
    [
        React.createElement('h1', { style: { backgroundColor: 'blue',width: '250px',height:'250px',color:'white' } }, 'White on Blue!'),
        React.createElement('h1', { style: { backgroundColor: 'red',width: '250px',height:'250px',color:'blue' } }, 'Blue on Red!'),
        React.createElement('h1', { style: { backgroundColor: 'pink',width: '250px',height:'250px',color:'green' } }, 'Green on Pink!'),
    ],
    document.getElementById('app')
);
