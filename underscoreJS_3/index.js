var myState = {
    name: "Washington",
    capital: "Olympia",
    population: 7062000,
    nickname: "The Evergreen State"
};
var businesses = [
    {
        name: "Coding Dojo",
        city:  "Bellevue"
    },
    {
        name: "Facebook",
        city: "Mountain View"
    },
    {
        name: "Microsoft",
        city: "Redmond"
    },
    {
        name: "Expedia",
        city: "Bellevue"
    }
];

// mystate key and value
mystateKey = _.map(myState, function(val,key){
    return key
})
mystateVal = _.map(myState, function(val,key){
    return val
})
console.log(mystateKey, mystateVal)
