window.onload = () =>{
    randomsGenerator()
}

function randomsGenerator(){
    var nums = [];
    var size = Math.floor(Math.random()*20);
    for (var i = 0; i < size; i++){
        nums.push(Math.floor(Math.random()*20));
    }
    buildChart(nums);
}

function buildChart(newdata){
    console.log(newdata)

    var bars = d3.select("#chart")
        .selectAll("div")
        .data(newdata)

    // update ( nowElement = newElement )
    bars.transition()
        .duration(2000)
        .text(function(d){ return d; })
        .style("height", function(d){ return d*20+"px" })
        .style("background-color", "lightblue")

    // enter ( nowElement < newElement )
    bars.enter().append("div")
        .transition()
        .duration(2000)
        .text(function(d){ return d; })
        .style("height", function(d){ return d*20+"px" })
        .style("background-color", "lightblue")

    // exit ( nowElement > newElement )
    bars.exit()
        .transition()
        .duration(2000)
        .style("height", function(d){ return '0px' })
        .style("background-color", "gray")
        .remove();
}
