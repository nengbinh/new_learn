var ironman = {
    name: "Tony Stark",
    alias: "Iron Man"
};
var avengers_array = [
    {name: "Tony Stark", alias: "Iron Man", ability: "flight"},
    {name: "Bruce Banner", alias: "The Incredible Hulk", ability: "superhuman strength"},
    {name: "Steve Rogers", alias: "Captain America", ability: "superhuman strength"},
    {name: "Clint Barton", alias: "Hawkeye", ability: "superior hand-eye coordination"},
    {name: "Thor Odinson", alias: "Thor", ability: "teleportation"},
    {name: "Natasha Romanova", alias: "Black Widow", ability: "espionage"}
];

// 遍历数组
_.each(ironman, function(value, key){
    console.log(key+': '+value);
})
// name: Tony Stark
// index.js:14 alias: Iron Man

// 这个跟ES6的一样
var avengersAssemble = _.map(avengers_array, function(avenger){ return avenger; });

// 获取数组里面的所有name
var avengerNames = _.pluck(avengers_array, 'name');
// 0: "Tony Stark"
// 1: "Bruce Banner"
// 2: "Steve Rogers"
// 3: "Clint Barton"
// 4: "Thor Odinson"
// 5: "Natasha Romanova"

// 查找符合条件的所有数据
var superhumanStrengthAvengersArray = _.where(avengers_array, {ability: "superhuman strength"});
// 0: {name: "Bruce Banner", alias: "The Incredible Hulk", ability: "superhuman strength"}
// 1: {name: "Steve Rogers", alias: "Captain America", ability: "superhuman strength"}

// 返回符合条件的第一个数据 找不到就输出undefined
var superhumanStrengthAvenger = _.findWhere(avengers_array, {ability: "superhuman strength"});
//{name: "Bruce Banner", alias: "The Incredible Hulk", ability: "superhuman strength"}

// 返回满足条件的所有数据
var superhumanStrengthAvengers = _.filter(avengers_array,
                                        function(avenger){
                                            return avenger.ability == "superhuman strength";
                                        }
                                    );
//0: {name: "Bruce Banner", alias: "The Incredible Hulk", ability: "superhuman strength"}
// 1: {name: "Steve Rogers", alias: "Captain America", ability: "superhuman strength"}

// 排序
var sortedAvengerNames = _.sortBy(avengers_array, 'name');
// 0: {name: "Bruce Banner", alias: "The Incredible Hulk", ability: "superhuman strength"}
// 1: {name: "Clint Barton", alias: "Hawkeye", ability: "superior hand-eye coordination"}
// 2: {name: "Natasha Romanova", alias: "Black Widow", ability: "espionage"}
// 3: {name: "Steve Rogers", alias: "Captain America", ability: "superhuman strength"}
// 4: {name: "Thor Odinson", alias: "Thor", ability: "teleportation"}
// 5: {name: "Tony Stark", alias: "Iron Man", ability: "flight"}
