var ironman = {
    name: "Tony Stark",
    alias: "Iron Man"
};
var avengers_array = [
    {name: "Tony Stark", alias: "Iron Man", ability: "flight"},
    {name: "Bruce Banner", alias: "The Incredible Hulk", ability: "superhuman strength"},
    {name: "Steve Rogers", alias: "Captain America", ability: "superhuman strength"},
    {name: "Clint Barton", alias: "Hawkeye", ability: "superior hand-eye coordination"},
    {name: "Thor Odinson", alias: "Thor", ability: "teleportation"},
    {name: "Natasha Romanova", alias: "Black Widow", ability: "espionage"}
];

// Array

// returns the first value in an array
var firstAvenger = _.first(avengers_array);
// The optional second parameter allows for additional values to be returned, but when specified, comes back as an array
var lastTwoAvengers = _.last(avengers_array, 2);
// returns an array with the values from argument1 (inclusive) to argument2 (exclusive)
var newArray = _.range(0,10);


// OBJECT

// .defaults()
var almostAvenger = {
    name: "John Q. Public",
    alias: "John Q"
};
_.defaults(almostAvenger, {
    name: "Need a Real Name",
    alias: "Need a Superhero Name",
    abilities: "Need Superhero Abilities"
});

// The .defaults() function is a great one.
// What it does is supplies defaults to an object if they haven’t already been created.
// It’s kind of like a fail-safe for your apps. Our almostAvenger object will look like this:

// {
//     name: "John Q. Public",
//         alias: "John Q",
//     abilities: "Need Superhero Abilities"
// }
