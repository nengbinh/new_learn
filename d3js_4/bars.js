// you will need to provide a lot more code, but here are your starting dimensions & two sets of data
var height = 500;
var width = 800;
var margins = {
    left: 40,
    top: 40,
    right: 40,
    bottom: 40
}
var barData = [ // meant for starting image
    {strength:38},
    {strength:13},
    {strength:24},
    {strength:5},
    {strength:48},
    {strength:30},
    {strength:16},
    {strength:43},
    {strength:14},
    {strength:8},
    {strength:22},
    {strength:35}
]
var barData2 = [  // transition to this set of data after 5 seconds
    {strength:13},
    {strength:5},
    {strength:22},
    {strength:16},
    {strength:40},
    {strength:49},
    {strength:27},
    {strength:28},
    {strength:34},
    {strength:19},
    {strength:39},
    {strength:35}
]

//

var xscale = d3.scaleLinear()
    .domain([0,12])
    .range([0,width-margins.left-margins.right]);
var yscale = d3.scaleLinear()
    .domain([0,50])
    .range([height-margins.top-margins.bottom,0]);
var xaxis = d3.axisBottom(xscale)
    .ticks(10);
var yaxis = d3.axisLeft(yscale)
    .ticks(10);

// the bar width
var barWidth = Math.floor((width-margins.left-margins.right)/barData.length);
//
var svg = d3.select("svg")
    .attr("width", width)
    .attr("height", height)

svg.append("g")
    .attr("transform","translate("+margins.left+","+margins.top+")")
    .call(yaxis);
svg.append("g")
    .attr("transform","translate("+margins.left+","+(height-margins.bottom)+")")
    .call(xaxis);
//
function showdata(data) {
    x = 0
    svg = d3.select("svg")
        .selectAll('rect')
        .data(data)

    svg.transition().duration(2000)
        .attr('x', () => {
            x++
            return xscale(x) + 10
        })
        .attr('y', d => (yscale(0) + 40) - yscale(d.strength))
        .attr('width', barWidth)
        .attr('height', d => yscale(d.strength))

    svg.enter().append('rect')
        .transition()
        .duration(2000)
        .attr('x', () => {
            x++
            return xscale(x) + 10
        })
        .attr('y', d => (yscale(0) + 40) - yscale(d.strength))
        .attr('width', barWidth)
        .attr('height', d => yscale(d.strength))
}

showdata(barData)

setTimeout(function(){
    showdata(barData2)
}, 3000)
